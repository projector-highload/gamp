<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Event\CommentCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use TheIconic\Tracking\GoogleAnalytics\Analytics;

class MeasurementEventSubscriber implements EventSubscriberInterface
{
    private const PROTO_VERSION = '1';

    private const TRACKING_CLIENT_ID = 'UA-46231186-3';

    private Analytics $analytics;

    private string $clientId;

    public function __construct()
    {
        $this->analytics = (new Analytics(true))
            ->setProtocolVersion(self::PROTO_VERSION)
            ->setTrackingId(self::TRACKING_CLIENT_ID)
            ->setAsyncRequest(true);

        $this->clientId = '';
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onRequestEvent',
            TerminateEvent::class => 'onKernelTerminate',
            CommentCreatedEvent::class => 'trackAction',
        ];
    }

    public function trackAction(CommentCreatedEvent $event): void
    {
        $this->analytics
            ->setClientId($this->clientId)
            ->setUserId((string) $event->getComment()->getAuthor()->getId())
            ->setEventAction('comment_left')
            ->setEventCategory('comments')
            ->setEventLabel('comment_left')
            ->setEventValue($event->getComment()->getId())
            ->sendEvent();
    }

    public function onRequestEvent(RequestEvent $event): void
    {
        if ('' === $gid = (string) $event->getRequest()->cookies->get('_ga', '')) {
            return;
        }

        $this->clientId = str_replace('GA1.2.', '', $gid);

        return;
    }

    public function onKernelTerminate(): void
    {
        $this->clientId = '';
    }
}
